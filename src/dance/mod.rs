use std::collections::{HashMap, hash_map};
use yaml_rust::Yaml;

pub mod tech;
use self::tech::Tech;

pub struct Dance {
    title: String,
    version: String,
    techniques: HashMap<String, Tech>,
}

impl Dance {
    fn new() -> Self {
        Self {
            title: String::new(),
            version: String::new(),
            techniques: HashMap::new(),
        }
    }

    pub fn techniques(&self) -> hash_map::Values<String, Tech> {
        self.techniques.values()
    }

    fn set_title_from(&mut self, doc: &Yaml) {
        self.title = yaml_to_string(doc);
    }

    fn set_version_from(&mut self, doc: &Yaml) {
        self.version = yaml_to_string(doc);
    }

    fn set_techniques_from(&mut self, doc: &Yaml) {
        match doc {
            Yaml::Hash(ref map) => {
                for (key, value) in map {
                    let tech = Tech::from((key, value));
                    let tech_id = tech.get_id().to_string();
                    match self.techniques.insert(tech_id.clone(), tech) {
                        None => (),
                        _ => panic!("Technique \"{}\" is added twice in the configuration file",
                                    tech_id),
                    }
                }
            },
            _ => panic!("Unexpected technique value type: expecting Map"),
        }
    }
}

fn yaml_to_string(doc: &Yaml) -> String {
    match doc {
        Yaml::String(ref s) => s.to_string(),
        Yaml::Real(ref s) => s.to_string(),
        _ => panic!("Unexpected value type: expecting string"),
    }
}

impl<'a> From<&'a Yaml> for Dance {
    fn from(doc: &'a Yaml) -> Self {
        let mut ret = Self::new();
        match doc {
            Yaml::Hash(ref map) => {
                for (key, value) in map {
                    match key {
                        Yaml::String(ref key) => {
                            match key.as_ref() {
                                "title" => ret.set_title_from(value),
                                "version" => ret.set_version_from(value),
                                "techniques" => ret.set_techniques_from(value),
                                _ => (),
                            }
                        },
                        _ => panic!("Unexpected Map key type: expecting string"),
                    }
                }
            }
            _ => panic!("Unexpected data type: expecting Map"),
        }
        ret
    }
}
