use yaml_rust::Yaml;

pub struct Tech {
    id: String,
    req: Vec<String>,
}

impl Tech {
    pub fn get_id(&self) -> &str {
        &self.id[..]
    }
}

impl<'a> From<(&'a str, &'a Yaml)> for Tech {
    fn from((name, doc): (&'a str, &'a Yaml)) -> Self {
        let mut tech = Self { id: name.to_string(), req: Vec::new() };

        match doc {
            Yaml::Null => (),
            Yaml::Array(ref a) => {
                for t in a {
                    match t {
                        Yaml::String(ref s) => {
                            tech.req.push(s.to_string());
                        },
                        _ => panic!("Yaml parser: Unexpected technique value type (expecting String)"),
                    }
                }
            },
            _ => panic!("Yaml parser: Unexpected technique description type (expecting array)"),
        }
        tech
    }
}

impl<'a> From<(&'a Yaml, &'a Yaml)> for Tech {
    fn from((key, value): (&'a Yaml, &'a Yaml)) -> Self {
        match key {
            Yaml::String(ref s) => {
                Self::from((&s[..], value))
            },
            _ => panic!("Yaml parser: Unexpected technique value type (expecting String)"),
        }
    }
}
