extern crate clap;
extern crate dance_therapy;

use clap::{Arg, App, SubCommand};

fn main() {
    let matches = App::new("Dance Therapy")
        .version("0.1.0")
        .author("Albin Kauffmann <albin@kauff.org>")
        .about("Avoid headaches by helping you to list dance techniques and moves")
        .arg(Arg::with_name("database")
             .long("database")
             .value_name("FOLDER")
             .help("Sets the dance database folder path")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("dance")
             .long("dance")
             .value_name("DANCE")
             .help("Sets the dance name (e.g. \"wcs\")")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("v")
             .short("v")
             .multiple(true)
             .help("Sets the level of verbosity"))
        .arg(Arg::with_name("l")
             .short("l")
             .long("lang")
             .value_name("LANG")
             .help("Sets the language tag (follows the BCP47 definition, e.g.: en-US, fr, ...)")
             .takes_value(true))
        .subcommand(SubCommand::with_name("list")
                    .about("List elements of the database")
                    .subcommand(SubCommand::with_name("techniques")
                                .about("Display the techniques listed in the database"))
                    .subcommand(SubCommand::with_name("moves")
                                .about("Display the moves listed in the database")))
        .get_matches();

    dance_therapy::run(&matches);
}
