extern crate clap;
extern crate fluent;
extern crate fluent_locale;
extern crate yaml_rust;

use std::fs;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::{Path, PathBuf};
use clap::ArgMatches;
use fluent::bundle::FluentBundle;
use fluent_locale::{negotiate_languages, NegotiationStrategy};
use yaml_rust::{Yaml, YamlLoader};

mod dance;
use self::dance::Dance;

fn cmd_list(d: &Dance, fbundle: &FluentBundle, sub_m: &ArgMatches) {
    match sub_m.subcommand() {
        ("techniques", _) => {
            for t in d.techniques() {
                match fbundle.format(t.get_id(), None) {
                    Some((translation, _)) => {
                        println!("{}: {}", t.get_id(), translation);
                    },
                    None => println!("{}", t.get_id()),
                }
            }
        },
        _ => panic!("Unexpected \"list\" sub arguments"),
    }
}

fn read_file(path: &Path) -> Result<String, io::Error> {
    let mut f = File::open(path)?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}

fn db_get_yaml(db_folder: &str, dance_name: &str) -> Yaml {
    let db_folder = Path::new(db_folder);

    if !db_folder.is_dir() {
        panic!("Please provide a folder to the --database option");
    }

    let mut db_yaml = db_folder.to_path_buf();
    db_yaml.push(format!("{}.yaml", dance_name));
    let db_yaml = db_yaml.as_path();
    if !db_yaml.is_file() {
        match db_folder.to_str() {
            Some(f) => panic!("The {}.yaml file does not exist in {}",
                              dance_name, f),
            None => panic!("The {}.yaml file does not exist in your database folder",
                           dance_name),
        }
    }

    let s = read_file(&db_yaml).unwrap();

    let mut yaml = YamlLoader::load_from_str(&s)
        .expect("Failed to load yaml file");
    if yaml.len() != 1 {
        panic!("The yaml file contains an unexpected number of documents (it \
                should only contain one document)");
    }
    yaml.remove(0)
}

/// This helper function allows us to read the list
/// of available locales by reading the list of
/// directories in `lang_folder`.
///
/// It is expected that every directory inside it
/// has a name that is a valid BCP47 language tag.
fn get_available_locales(lang_folder: &Path) -> Result<Vec<String>, io::Error> {
    let mut locales = vec![];

    let res_dir = fs::read_dir(lang_folder)?;
    for entry in res_dir {
        if let Ok(entry) = entry {
            let path = entry.path();
            if path.is_dir() {
                if let Some(name) = path.file_name() {
                    if let Some(name) = name.to_str() {
                        locales.push(String::from(name));
                    }
                }
            }
        }
    }
    return Ok(locales);
}

/// This function negotiates the locales between available
/// and requested by the user.
///
/// It uses `fluent-locale` library but one could
/// use any other that will resolve the list of
/// available locales based on the list of
/// requested locales.
fn get_app_locales(lang_folder: &Path, requested: &[&str]) -> Result<Vec<String>, io::Error> {
    let available = get_available_locales(lang_folder)?;
    let resolved_locales = negotiate_languages(
        requested,
        &available,
        Some("en-US"),
        &NegotiationStrategy::Filtering,
    );
    return Ok(resolved_locales
        .into_iter()
        .map(|s| String::from(s))
        .collect::<Vec<String>>());
}

fn lang_folder_new(db_folder: &str) -> PathBuf {
    // 1. Construct the lang_folder directory
    let db_folder = Path::new(db_folder);
    if !db_folder.is_dir() {
        panic!("Please provide a folder to the --database option");
    }
    let mut lang_folder = db_folder.to_path_buf();
    lang_folder.push("lang");
    {
        let lang_folder = lang_folder.as_path();
        if !lang_folder.is_dir() {
            match lang_folder.to_str() {
                Some(f) => panic!("The {} directory does not exist", f),
                None => panic!("The lang directory does not exist in your database folder"),
            }
        }
    }
    lang_folder
}

fn locales_new(lang_folder: &PathBuf, lang: Option<&str>) -> Vec<String> {
    // 2. If the argument length is more than 1,
    //    take the second argument as a comma-separated
    //    list of requested locales.
    //
    //    Otherwise, take ["en-US"] as the default.
    let requested = lang
        .map_or(vec!["en-US"], |arg| arg.split(",").collect());

    // 3. Negotiate it against the available ones
    let locales = get_app_locales(lang_folder, &requested)
        .expect("Failed to retrieve available locales");
    locales
}

fn fbundle_init<'a>(lang_folder: &PathBuf, dance_name: &str,
                    locales: &'a Vec<String>) -> FluentBundle<'a> {
    // 4. Create a new Fluent FluentBundle using the
    //    resolved locales.
    let mut bundle = FluentBundle::new(&locales);

    // 5. Load the localization resource
    let mut full_path = lang_folder.clone();
    full_path.push(&locales[0]);
    full_path.push(format!("{}.ftl", dance_name));
    let res = read_file(&full_path).unwrap();
    // 5.1 Insert the loaded resource into the
    //     Fluent FluentBundle.
    bundle.add_messages(&res).unwrap();
    bundle
}

pub fn run(matches: &ArgMatches) {
    let db_folder = matches.value_of("database").unwrap();
    let dance_name = matches.value_of("dance").unwrap();

    let yaml = db_get_yaml(db_folder, dance_name);
    let d = Dance::from(&yaml);

    let lang = matches.value_of("l");
    let lang_folder = lang_folder_new(db_folder);
    let locales = locales_new(&lang_folder, lang);
    let fbundle = fbundle_init(&lang_folder, dance_name, &locales);

    match matches.subcommand() {
        ("list", Some(sub_m)) => cmd_list(&d, &fbundle, sub_m),
        _ => panic!("Unexpected command line argument"),
    }
}
