# Dance Therapy

_Dance Therapy_ is a program that helps dance addicts or teachers to list dance
techniques and moves along with there descriptions.
The future idea of this project is to be able to:

* easily find the technical requirements of a dance move
* find the dance moves that composes a more complex move
* maybe even define the content of a dance class

## Build the project

To build this project, you need a Rust compiler and Cargo.

If you are not familiar with Rust, you can find installation instructions on
[this page](https://doc.rust-lang.org/book/2018-edition/ch01-01-installation.html).
Note that if you are running Linux, your distribution probably have a _rust_ or
_rustup_ package.

Once installed, you just need to build the project with:

    cargo build

## Create a database

The database is a directory containing a set of:

* [YAML](http://yaml.org/spec/1.2/spec.html) files (one `*.yaml` file per dance
type) for dance techniques and moves definitions
* [Fluent](https://projectfluent.org) files for translations (`*.ftl` files)

So a database for the _West Coast Swing_ dance (abbreviated _wcs_) would have
the following file tree:

    .
    |-- wcs.yaml
    `-- lang
        |-- en-US
        |   `-- wcs.ftl
        `-- fr
            `-- wcs.ftl

TODO: describe the `*.yaml` and `*.ftl` files format.

## Use the program

The program provides a good `--help` option, so please run the following command
to get the usage of `dance-therapy`:

    cargo run -- --help

An example of usage could be something like:

    cargo run -- --database /path/to/database --dance wcs list techniques

## Why did I create this project?

I started this project to learn Rust and I hope to use it when preparing
[West Coast Swing](https://en.wikipedia.org/wiki/West_Coast_Swing) dance classes
(for beginners).

As of September 2018, this project is still new and under active development.
